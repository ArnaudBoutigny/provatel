---
title: 'Videosurveillance'
date: 2018-11-18T12:33:46+10:00
draft: false
weight: 1
heroHeading: 'Videosurveillance'
heroSubHeading: 'Installations et maintenance'
heroBackground: 'work/service2.jpg'
thumbnail: 'work/work1-thumbnail.jpg'
images: ['https://source.unsplash.com/random/400x600/?nature', 
'https://source.unsplash.com/random/400x300/?travel','https://source.unsplash.com/random/400x300/?architecture','https://source.unsplash.com/random/400x600/?buildings','https://source.unsplash.com/random/400x300/?city','https://source.unsplash.com/random/400x600/?business']
---

Une large gamme de produits, adaptés aussi bien à une clientèle grand public que professionnelle.
Nos systèmes de vidéosurveillance, vous permettent de visionner les images et les évènements qui se déroulent sur le site protégé, de jour comme de nuit, en intérieur comme en extérieur.

## Du matériel de qualité

Les caméras diffusent des images claires en haute définition avec un grand champ de vision. Raccordées à un enregistreur numérique avec disque dur, les images capturées par les caméras de surveillance peuvent s’archiver directement dans l’enregistreur vidéo pour être consultées ultérieurement.

PROVATEL sera en mesure de vous proposer une solution fiable et adaptée à vos besoins.

N’hésitez pas à me contacter afin de construire votre projet ensemble.