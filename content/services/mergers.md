---
title: 'Videosurveillance'
date: 2018-11-28T15:15:34+10:00
icon: 'services/videosurveillance.png'
featured: true
draft: false
heroHeading: 'Videosurveillance'
heroSubHeading: 'Installations et maintenance.'
heroBackground: 'services/service2.jpg'
images: ['services/cameradome.jpg','services/cameralongue.jpg','services/enregistreur.jpg','services/disquedur.jpg','services/moniteur.jpg','services/onduleur.jpg']


---

Une large gamme de produits, adaptés aussi bien à une clientèle grand public que professionnelle.
Nos systèmes de vidéosurveillance, vous permettent de visionner les images et les évènements qui se déroulent sur le site protégé, de jour comme de nuit, en intérieur comme en extérieur.

## Du matériel de qualité

Les caméras diffusent des images claires en haute définition avec un grand champ de vision. Raccordées à un enregistreur numérique avec disque dur, les images capturées par les caméras de surveillance peuvent s’archiver directement dans l’enregistreur vidéo pour être consultées ultérieurement.

PROVATEL sera en mesure de vous proposer une solution fiable et adaptée à vos besoins.

N’hésitez pas à me contacter afin de construire votre projet ensemble.
